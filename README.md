# This demos the broken pipeline

On commit 03b772c2c039022d5b2b4b22af1a3324e2048da9, I added 3 files containing 1 line of code each

On commit 56cdeef7935f5d55615dd71c9211e135cc1611f6, I removed 3 files containing 1 line of code each.

On commit 8f76adc875d0cc03c1aeb2fddcb0f4dc8d6eeb58, I updated the documentation file.

`git diff 03b772c2c039022d5b2b4b22af1a3324e2048da9 56cdeef7935f5d55615dd71c9211e135cc1611f6` shows that there are only 3 deletions between these commits. Yet when running aws-codeguru-cli with `--commit-range 03b772c2c039022d5b2b4b22af1a3324e2048da9:56cdeef7935f5d55615dd71c9211e135cc1611f6`, I get a scan that checks 6 lines of code. Why?

Also when running aws-codeguru-cli with `--commit-range 56cdeef7935f5d55615dd71c9211e135cc1611f6:8f76adc875d0cc03c1aeb2fddcb0f4dc8d6eeb58`, I get a scan that checks 6 lines of code. Why? Not only are there not 6 lines of code in these commits, I didn't change anything but README.md.